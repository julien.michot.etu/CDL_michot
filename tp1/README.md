# CDL Michot

## Getting started

Install node modules :

```
npm install
```

To run the app, you have 3 choices :

- Pass args directly in the command line `node tp1.js --configuration $CONF_FILE`
- Use env var : first export the var `export FICHIER_DE_CONFIGURATION="$CONF_FILE"` then run `node tp1.js`
- Use file written in the source code by just running `node tp1.js`

`$CONF_FILE` can be `config.yml` or `config.json`
