import { load } from 'js-yaml';
import { readFileSync } from 'fs';

function readConfFile() {
  const args = process.argv;
  let confFile = '';

  if (args[2] == '--configuration' && args[3] != null) {
    confFile = args[3];
  } else if (process.env.FICHIER_DE_CONFIGURATION != null) {
    confFile = process.env.FICHIER_DE_CONFIGURATION;
  } else {
    confFile = 'config.yml';
  }

  readFile(confFile);
}

function readFile(file) {
  const fileExtension = file.split('.')[1];
  if (fileExtension == 'yml') {
    try {
      const result = load(readFileSync(confFile));
      console.log(result);
    } catch (e) {
      console.error(e);
    }
  } else if (fileExtension == 'json') {
    const jsonFile = require('./' + file);
    console.log(jsonFile);
  }
}

readConfFile();
