#!/bin/sh

execute()
{
  echo $*
  $*
}

execute mvn clean compile

execute mvn exec:java -Dexec.mainClass="trois_tiers.Main"

execute mvn exec:java -Dexec.mainClass="helloworld.Main"

execute mvn exec:java -Dexec.mainClass="trois_tiers.App"
