package trois_tiers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class App {

    @Autowired
    Presentation presentation;

    @Autowired
    Metier metier;

    @Autowired
    Stockage stockage;

    public static void main(String[] args) {
        ConfigurableApplicationContext appContext = getAppContext();
        App app = appContext.getBean(App.class);
        appContext.close();
    }

    public static ConfigurableApplicationContext getAppContext() {
        return new ClassPathXmlApplicationContext("spring-config.xml");
    }

    public void setPresentation(Presentation presentation) {
        this.presentation = presentation;
    }

    public void setMetier(Metier metier) {
        this.metier = metier;
    }

    public void setStockage(Stockage stockage) {
        this.stockage = stockage;
    }
}
