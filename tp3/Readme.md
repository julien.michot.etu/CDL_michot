# TP3

## Docker Compose

> Commands to start :

```bash
docker-compose up
```

## Terraform

> Commands to deploy :
```bash
terraform init
terraform plan (to see what will be done, can be skipped)
terraform apply
```

> Commands to undeploy
```bash
terraform destroy
```

## Ansible

> Commands to deploy :

```bash
ansible-playbook deploy-wordpress.yml
```

> Commands to undeploy
The only way i found to undeploy is to stop the containers
```bash
docker stop containerIDr
```

## Helm

Kubernetes and Helm must be installed

> Commands to deploy :
```bash
helm repo add my-repo https://charts.bitnami.com/bitnami
helm install my-release my-repo/wordpress
```

kubectl commands are prompted to get url (for me localhost worked)

If you want to connecty as admin you will get username & password

Docker containers are visible with `docker ps` and accessible through `docker exec -ti containerID bash`

> Commands to undeploy
```bash
helm delete my-release
```


